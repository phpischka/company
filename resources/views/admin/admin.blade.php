@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard Administrator</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">theme</th>
                            <th scope="col">message</th>
                            <th scope="col">name</th>
                            <th scope="col">email</th>
                            <th scope="col">created_at</th>
                            <th scope="col">Document</th>
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($contacts as $contact)
                                <tr>
                                  <th scope="row">{{$contact->id}}</th>
                                  <td>{{$contact->theme}}</td>
                                  <td>{{$contact->message}}</td>
                                  <td>{{$contact->name}}</td>
                                  <td>{{$contact->email}}</td>
                                  <td>{{$contact->created_at}}</td>
                                  <td>
                                      @if($contact->file)
                                        <a href="{{url('storage/'. $contact->file)}}">Document</a>
                                      @endif
                                  </td>
                                  <td>
                                      <input data-id="{{$contact->id}}" class="status" type="checkbox" name="status" value="{{$contact->status ? 1 : 0}}" {{$contact->status ? 'checked' : ''}}>
                                  </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$contacts->appends(request()->all())->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
