@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Contact Form</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form action="{{route('send')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="theme">Theme</label>
                            <input type="text" name="theme" value="{{old('theme')}}" class="form-control" id="theme"  placeholder="Enter theme">
                            @error('theme')
                                <small id="emailHelp" class="form-text text-muted">{{ $message }}</small>
                            @enderror
                        </div>
                       
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea name="message" class="form-control" id="message" rows="3" placeholder="Enter message">{{old('message')}}</textarea>
                            @error('message')
                                    <small id="emailHelp" class="form-text text-muted">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="file">Document</label>
                            <input type="file" name="file" class="form-control" id="file"  placeholder="Enter file">
                            @error('file')
                                <small id="emailHelp" class="form-text text-muted">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
