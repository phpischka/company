
        <H2>Message from Test site.</H2>
    
        <table width="100%" border="0"  cellpadding="2" style="background-color: #E6E6E6">

            <tbody>
                @isset($feedback['id'])
                    <tr>
                        <td with="50">id</td>
                        <td>{{ $feedback['id']}}</td>
                    </tr>
                @endisset
                @isset($feedback['theme'])
                    <tr>
                        <td with="50">Theme</td>
                        <td>{{ $feedback['theme']}}</td>
                    </tr>
                @endisset
                @isset($feedback['message'])
                    <tr>
                        <td with="50">Message</td>
                        <td>{{ $feedback['message']}}</td>
                    </tr>
                @endisset
                @isset($feedback['name'])
                  <tr>
                        <td with="50">Name</td>
                        <td>{{ $feedback['name'] }}</td>
                  </tr>      
                @endisset
                @isset($feedback['email'])
                    <tr>
                        <td with="50">Email</td>
                        <td>{{ $feedback['email']}}</td>
                    </tr>
                @endisset

                @isset($feedback['file'])
                    <tr>
                        <td with="50">File</td>
                        <td><a href="{{url('storage/' . $feedback['file'])}}">Document</a></td>
                    </tr>
                @endisset
                @isset($feedback['created_at'])
                    <tr>
                        <td with="50">created_at</td>
                        <td>{{ $feedback['created_at']}}</td>
                    </tr>
                @endisset
            </tbody>
        </table>

