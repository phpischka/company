<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'status', 'theme', 'message', 'name', 'email', 'file',
    ];
}
