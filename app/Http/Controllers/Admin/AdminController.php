<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index()
    {
        //Получение списка заявок
        $contacts = Contact::paginate(10);
        
        return view('admin.admin', compact('contacts'));
    }
}
