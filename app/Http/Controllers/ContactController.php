<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendContactEmail;
use Carbon\Carbon;

class ContactController extends Controller
{
    private $hours = 24;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function index()
    {
        return view('contact');
    }
    
     public function send(Request $request)
    {
        $request->validate([
            'theme' => 'required|max:255',
            'message' => 'required|max:1000',
            'file' => 'file|mimes:txt,pdf|max:512',
        ]);
        
        $user = Auth::user();
        
        if (self::intervalTimeSend($this->hours, $user)) {
            $time = self::intervalTimeSend($this->hours, $user);
            
            return \Redirect::back()->with('status', 
                    'Re-application can be left in ' . $time . ' hours'); 
        }
        
        //Получение данных от пользователя с формы
        $theme = $request->input('theme');
        $message = $request->input('message');
        $name = $user->name;     //Иначе можно создать hidden field
        $email = $user->email;   // И там эти данные вставить, а тут получить
        $file = $request->file('file');
        
       try {
             //Сохранение файла на диск
            if ($file) {
                $filePath = Storage::putFile('documents', new File($file), 'public');
            }else{
                $filePath = '';
            }
            
            //Сохранение контактов и ссылку на файл в БД
            $contact = new Contact();
            $contact->theme = $theme;
            $contact->message = $message;
            $contact->name = $name;
            $contact->email = $email;
            $contact->file = $filePath;
            
            $contact->save();
            
            //подготовка данных к отправке на майл админу
            $emails = 'admin1@email';
            
            $feedback = [
                'id' => $contact->id,
                'theme' => $theme,
                'message' => $message,
                'name' => $name,
                'email' => $email, 
                'file' => $filePath, 
                'created_at' => $contact->created_at,
            ];
            
             //Отправка $emails в очередь
            $job = (new SendContactEmail($emails, $feedback))
                ->delay(Carbon::now()->addMinutes(1));
    
            dispatch($job);
            
        } catch (\Exception $e) {
            return \Redirect::back()->with('status', 'Application not sent!');
        }
        
        return \Redirect::back()->with('status', 'Application has been successfully sent!');
    }
    
    public function changeStatusAjax(Request $request)
    {
        $contact = Contact::where('id', $request->id)->first();
        $contact->status = $request->status;
        $contact->save();
        
        return 200;
    }
    
    public function intervalTimeSend ($hours, $user)
    {
        $entry = Contact::where('email', $user->email)->first();
        
        //проверка существуют ли сообщения от текущего пользователя в БД
        if ($entry) {
            $contact = Contact::where('email', $user->email)->orderBy('created_at', 'desc')->take(1)->first(); 
            //Подчет прошедшего времени от последнего сообщения
            $diference = strtotime(Carbon::now()) - strtotime($contact->created_at);
            //Сколько должно пройти времени до следующей отправки сообщения
            $intervalSeconds = $hours * 3600;
            
            if ($diference < $intervalSeconds) {
                //сколько осталось времени до следующей отправки сообщения
                $timeLeftSeconds = $intervalSeconds - $diference;
                //результат
                return Carbon::createFromTimestamp($timeLeftSeconds)->format('H:i');
            } else {
                return false;
            }
  
        }else{
            return false;
        }
    }
}
