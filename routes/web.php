<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\ContactController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin');
});

Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::post('/send', [ContactController::class, 'send'])->name('send');
Route::post('/changeStatusAjax', [ContactController::class, 'changeStatusAjax']);



