/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

$('.status').on('change', function () {
    this.value = this.checked ? '1' : '0';
    var status = this.value;
    var id = $(this).data('id');
    
    $.ajax({
        method: "POST",
        url: "/changeStatusAjax",
        data: { 
            id: id,
            status: status
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status === 200) {
               arert('success')
            }
        }
    });
});